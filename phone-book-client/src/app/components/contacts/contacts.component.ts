import { Contact } from './../../models/contact';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  @Input() contacts: Contact[] = [];
  filteredContacts: Contact[] = [];
  constructor() {
    this.filteredContacts = [...this.contacts];
  }

  ngOnInit(): void {

  }

  resetContacts(contacts: Contact[]) {
    contacts = contacts.sort((a, b) => a.name.localeCompare(b.name));
    this.filteredContacts = [...contacts];
    this.contacts = [...contacts];
  }

  search(event) {
    const text = event.target.value.toLowerCase();
    if (text) {
      return this.filteredContacts = this.contacts.filter(c => c.name.toLowerCase().includes(text) ||
        c.number.toLowerCase().includes(text));
    }
    this.filteredContacts = [...this.contacts];
  }

}
