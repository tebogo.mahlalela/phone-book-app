import { PhoneBook } from './../../models/phone-book';
import { ContactsComponent } from './../contacts/contacts.component';
import { Contact } from './../../models/contact';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  selectedPhoneBook: PhoneBook = {} as any;
  @ViewChild(ContactsComponent) contactsComponent: ContactsComponent;

  constructor() { }

  ngOnInit(): void {
    this.selectedPhoneBook.contacts = [];
  }

  onReceivePhoneBook(phoneBook: PhoneBook) {
    this.selectedPhoneBook = phoneBook;
    this.contactsComponent.resetContacts(phoneBook.contacts ?? []);
  }

  onReceiveContact(contact: Contact) {
    this.selectedPhoneBook.contacts.push(contact);
    this.contactsComponent.resetContacts([...this.selectedPhoneBook.contacts]);
  }

}
