import { PhoneBooksService } from './../../services/phone-books.service';
import { Contact } from './../../models/contact';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  @Input() phoneBookId: number;
  @Output() sendContact: EventEmitter<Contact> = new EventEmitter();
  contactForm = new FormGroup({
    name: new FormControl('', Validators.required),
    number: new FormControl('', [Validators.required,
    Validators.minLength(3),
    Validators.maxLength(11),
    Validators.pattern('^[0-9]*$')
    ])
  });

  get formGroup() {
    return this.contactForm.controls;
  }

  constructor(private phoneBooksService: PhoneBooksService) { }

  ngOnInit(): void {
  }

  newContact() {
    if (!this.contactForm.valid) { return; }
    const contact: Contact = {
      id: 0,
      name: this.formGroup.name.value,
      number: this.formGroup.number.value,
      phoneBookId: this.phoneBookId
    };
    this.phoneBooksService.addContact(contact).subscribe((id) => {
      this.sendContact.emit({ ...contact, id });
    }, err => alert('An error occured'));
  }

}
