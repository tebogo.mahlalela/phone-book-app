import { Contact } from './../../models/contact';
import { PhoneBook } from './../../models/phone-book';
import { PhoneBooksService } from './../../services/phone-books.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-phone-books',
  templateUrl: './phone-books.component.html',
  styleUrls: ['./phone-books.component.scss']
})
export class PhoneBooksComponent implements OnInit {
  phoneBooks: PhoneBook[] = [];
  phoneBookName = '';
  phoneBookId: number;
  selectedPhoneBook: PhoneBook;

  @Output() sendContacts: EventEmitter<Contact[]> = new EventEmitter();
  @Output() sendPhoneBookId: EventEmitter<number> = new EventEmitter();
  @Output() sendPhoneBook: EventEmitter<PhoneBook> = new EventEmitter();

  constructor(private phoneBooksService: PhoneBooksService) { }

  ngOnInit(): void {
    this.phoneBooksService.getPhoneBooks().subscribe((phoneBooks) => {
      this.phoneBooks = phoneBooks;
      if (this.phoneBooks.length > 0) { this.onSelectPhoneBook(this.phoneBooks[0].id); }
    }, err => {
      console.log(err);
      alert('An error occured');
    });
  }

  onSelectPhoneBook(id: number) {
    this.phoneBooks = this.phoneBooks.map(p => ({ ...p, selected: id === p.id }));
    this.selectedPhoneBook = this.phoneBooks.find(p => p.id === id);
    this.sendPhoneBook.emit(this.selectedPhoneBook);
  }

  newPhoneBook() {
    if (this.phoneBookName.trim().length > 0) {
      this.phoneBooksService.addPhoneBook(this.phoneBookName).subscribe((id) => {
        this.phoneBooks.push({ name: this.phoneBookName, id, contacts: []});
        this.phoneBookName = '';
      }, err => alert('An error occured'));
    }
  }

}
