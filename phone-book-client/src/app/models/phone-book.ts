import { Contact } from './contact';
export interface PhoneBook {
    id: number;
    name: string;
    contacts: Contact[];
    selected?: boolean;
}
