import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { PhoneBook } from '../models/phone-book';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class PhoneBooksService {
  url = environment.API_URL;
  constructor(private http: HttpClient) { }

  getPhoneBooks(): Observable<PhoneBook[]> {
    return this.http.get<PhoneBook[]>(`${this.url}/phone-books`);
  }

  getContacts(id: number): Observable<Contact[]> {
    return this.http.get<Contact[]>(`${this.url}/contacts?phoneBookId=${id}`);
  }

  addContact(contact: Contact): Observable<number> {
    return this.http.post<number>(`${this.url}/contacts`, contact);
  }

  addPhoneBook(phoneBookName: string): Observable<number> {
    return this.http.post<number>(`${this.url}/phone-books?name=${phoneBookName}`, {});
  }
}
