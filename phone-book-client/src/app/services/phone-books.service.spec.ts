import { TestBed } from '@angular/core/testing';

import { PhoneBooksService } from './phone-books.service';

describe('PhoneBooksService', () => {
  let service: PhoneBooksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PhoneBooksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
