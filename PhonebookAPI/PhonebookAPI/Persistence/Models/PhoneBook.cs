﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace PhoneBookAPI.Persistence.Models
{
    public class PhoneBook
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public ICollection<Contact> Contacts { get; set; }

        public PhoneBook()
        {
            Contacts = new Collection<Contact>();
        }
    }
}