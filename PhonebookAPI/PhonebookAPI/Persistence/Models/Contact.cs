﻿using System.ComponentModel.DataAnnotations;

namespace PhoneBookAPI.Persistence.Models
{
    public class Contact
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [MinLength(3)]
        [MaxLength(15)]
        [Required]
        public string Number { get; set; }

        public int PhoneBookId { get; set; }
        public PhoneBook PhoneBook { get; set; }
    }
}