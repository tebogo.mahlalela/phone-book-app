﻿using PhoneBookAPI.Core.Services;
using PhoneBookAPI.Persistence.Context;

namespace PhoneBookAPI.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly PhoneBookContext _context;
        public IPhoneBookRepository PhoneBooks { get; set; }
        public IContactsRepository Contacts { get; set; }

        public UnitOfWork(PhoneBookContext context)
        {
            _context = context;
            PhoneBooks = new PhoneBookRepository(_context);
            Contacts = new ContactsRepository(_context);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}