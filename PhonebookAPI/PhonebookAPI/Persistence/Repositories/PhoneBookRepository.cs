﻿using System.Collections.Generic;
using PhoneBookAPI.Core.Services;
using PhoneBookAPI.Persistence.Context;
using PhoneBookAPI.Persistence.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace PhoneBookAPI.Persistence.Repositories
{
    public class PhoneBookRepository : IPhoneBookRepository
    {
        private readonly PhoneBookContext _context;

        public PhoneBookRepository(PhoneBookContext context)
        {
            _context = context;
        }

        public void Add(PhoneBook phoneBook)
        {
            _context.PhoneBooks.Add(phoneBook);
        }

        public PhoneBook Get(int id)
        {
            return _context.PhoneBooks.Find(id);
        }
        public IEnumerable<PhoneBook> GetAll()
        {
            return _context.PhoneBooks
                .Include(p => p.Contacts)
                .ToList();
        }

        public void Remove(PhoneBook phoneBook)
        {
            _context.PhoneBooks.Remove(phoneBook);
        }

        public PhoneBook GetByName(string name)
        {
            return _context.PhoneBooks.FirstOrDefault(phoneBook => phoneBook.Name == name);
        }
    }
}