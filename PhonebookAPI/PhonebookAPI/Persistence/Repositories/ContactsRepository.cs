﻿using Microsoft.EntityFrameworkCore;
using PhoneBookAPI.Core.Services;
using PhoneBookAPI.Persistence.Context;
using PhoneBookAPI.Persistence.Models;
using System.Linq;

namespace PhoneBookAPI.Persistence.Repositories
{
    public class ContactsRepository : IContactsRepository
    {
        private readonly PhoneBookContext _context;

        public ContactsRepository(PhoneBookContext context)
        {
            _context = context;
        }

        public Contact Get(int id)
        {
            return _context.Contacts
                .Include(c => c.PhoneBook)
                .FirstOrDefault(c => c.Id == id);
        }
    }
}