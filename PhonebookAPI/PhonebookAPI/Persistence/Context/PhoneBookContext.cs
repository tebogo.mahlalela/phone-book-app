﻿using Microsoft.EntityFrameworkCore;
using PhoneBookAPI.Persistence.Models;

namespace PhoneBookAPI.Persistence.Context
{
    public class PhoneBookContext : DbContext
    {
        public DbSet<PhoneBook> PhoneBooks { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        public PhoneBookContext(DbContextOptions<PhoneBookContext> options) : base(options)
        {
        }
    }
}