﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhoneBookAPI.Core.Resources;
using PhoneBookAPI.Core.Services;
using PhoneBookAPI.Persistence.Models;
using System.Collections.Generic;

namespace PhoneBookAPI.Controllers
{
    [Route("api/phone-books")]
    [ApiController]
    public class PhoneBooksController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PhoneBooksController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var phoneBooks = _unitOfWork.PhoneBooks.GetAll();
            var result = _mapper.Map<IEnumerable<PhoneBook>, IEnumerable<PhoneBookResource>>(phoneBooks);
            return Ok(result);
        }

        [HttpPost]
        public ActionResult Create([FromQuery] string name)
        {
            if (string.IsNullOrEmpty(name)) return BadRequest("Phone book name is required");

            var phoneBookFromDb = _unitOfWork.PhoneBooks.GetByName(name);
            if (phoneBookFromDb != null) return BadRequest($"Phone book '{name}' already exist");

            var phoneBook = new PhoneBook { Name = name };
            _unitOfWork.PhoneBooks.Add(phoneBook);
            _unitOfWork.SaveChanges();

            return Ok(phoneBook.Id);
        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult Delete(int id)
        {
            var phoneBook = _unitOfWork.PhoneBooks.Get(id);
            if (phoneBook == null) return NotFound();

            _unitOfWork.PhoneBooks.Remove(phoneBook);
            _unitOfWork.SaveChanges();

            return Ok();
        }

        [HttpPut]
        public ActionResult Update(PhoneBookResource resource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var phoneBook = _unitOfWork.PhoneBooks.Get(resource.Id);
            if (phoneBook == null) return NotFound();

            //Could use auto mapper if it was a big data model
            phoneBook.Name = resource.Name;
            _unitOfWork.SaveChanges();

            return Ok();
        }
    }
}