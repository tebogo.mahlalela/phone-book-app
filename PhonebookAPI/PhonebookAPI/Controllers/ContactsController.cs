﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhoneBookAPI.Core.Resources;
using PhoneBookAPI.Core.Services;
using PhoneBookAPI.Persistence.Models;
using System.Collections.Generic;

namespace PhoneBookAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ContactsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpPost]
        public ActionResult Create(ContactResource resource)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);
            var phoneBook = _unitOfWork.PhoneBooks.Get(resource.PhoneBookId);
            if (phoneBook == null) return NotFound();

            var contact = _mapper.Map<ContactResource, Contact>(resource);
            phoneBook.Contacts.Add(contact);
            _unitOfWork.SaveChanges();

            return Ok(contact.Id);
        }

        [HttpDelete]
        [Route("{id}")]
        public ActionResult Delete(int id)
        {
            var contact = _unitOfWork.Contacts.Get(id);
            if (contact == null) return NotFound();

            contact.PhoneBook.Contacts.Remove(contact);
            _unitOfWork.SaveChanges();

            return Ok();
        }
    }
}