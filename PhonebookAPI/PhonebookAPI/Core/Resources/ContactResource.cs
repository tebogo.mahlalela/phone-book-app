﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PhoneBookAPI.Core.Resources
{
    public class ContactResource
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [MinLength(3)]
        [MaxLength(15)]
        [Required]
        public string Number { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "PhoneBookId is required")]
        public int PhoneBookId { get; set; }
    }
}