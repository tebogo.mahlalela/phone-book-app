﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PhoneBookAPI.Core.Resources
{
    public class PhoneBookResource
    {
        [Required] public int Id { get; set; }

        [Required] [StringLength(50)] public string Name { get; set; }
        public IEnumerable<ContactResource> Contacts { get; set; }

        public PhoneBookResource()
        {
            Contacts = new List<ContactResource>();
        }
    }
}