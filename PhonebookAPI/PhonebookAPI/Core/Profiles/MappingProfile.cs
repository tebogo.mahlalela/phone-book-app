﻿using AutoMapper;
using PhoneBookAPI.Core.Resources;
using PhoneBookAPI.Persistence.Models;

namespace PhoneBookAPI.Core.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ContactResource, Contact>()
                .ForMember(c => c.Id, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<PhoneBookResource, PhoneBook>().ReverseMap();
        }
    }
}