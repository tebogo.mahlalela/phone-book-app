﻿using PhoneBookAPI.Persistence.Models;

namespace PhoneBookAPI.Core.Services
{
    public interface IContactsRepository
    {
        Contact Get(int id);
    }
}