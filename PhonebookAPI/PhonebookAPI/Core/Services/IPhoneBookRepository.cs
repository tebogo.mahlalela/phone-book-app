﻿using PhoneBookAPI.Persistence.Models;
using System.Collections.Generic;

namespace PhoneBookAPI.Core.Services
{
    public interface IPhoneBookRepository
    {
        void Add(PhoneBook phoneBook);

        PhoneBook Get(int id);

        IEnumerable<PhoneBook> GetAll();

        void Remove(PhoneBook phoneBook);

        PhoneBook GetByName(string name);
    }
}