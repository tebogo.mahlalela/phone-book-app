﻿namespace PhoneBookAPI.Core.Services
{
    public interface IUnitOfWork
    {
        IPhoneBookRepository PhoneBooks { get; set; }
        IContactsRepository Contacts { get; set; }

        void SaveChanges();
    }
}